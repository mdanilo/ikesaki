(function () {
    console.log('product json');
    console.log($price_avista);

    var jsonData = {
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "Executive Anvil",
        "image": "http://www.example.com/anvil_executive.jpg",
        "description": "Sleeker than ACME's Classic Anvil, the Executive Anvil is perfect for the business traveler looking for something to drop from a height.",
        "mpn": "925872",
        "brand": {
            "@type": "Thing",
            "name": "ACME"
        },
        "offers": {
            "@type": "Offer",
            "priceCurrency": "USD",
            "price": $price_avista,
            "priceValidUntil": "2020-11-05",
            "itemCondition": "http://schema.org/UsedCondition",
            "availability": "http://schema.org/InStock",
            "seller": {
                "@type": "Organization",
                "name": "Executive Objects"
            }
        }
    }

    var el = document.createElement('script');
    el.type = 'application/ld+json';
    el.innerHTML = JSON.stringify(jsonData);
    document.querySelector("body").appendChild(el);
})();